package com.softwaretest.airplanecalculate;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.softwaretest.airplanecalculate.dao")
@SpringBootApplication
public class AirplaneCalculateApplication {

    public static void main(String[] args) {
        SpringApplication.run(AirplaneCalculateApplication.class, args);
    }

}
