package com.softwaretest.airplanecalculate.bussiness;

import com.softwaretest.airplanecalculate.bean.GarbageInfo;

import java.util.List;

public class InnerCarryFreeGarbage {
    private float weight; // 可托运行李的总重量
    private float additianalWeight; // 附加的一件行李的重量

    public InnerCarryFreeGarbage() {
        this.weight=0;
        this.additianalWeight=0;
    }

    public InnerCarryFreeGarbage(float weight, float additianalWeight) {
        this.weight = weight;
        this.additianalWeight = additianalWeight;
    }

    // 国内可免费携带的行李重量
    public float InnerFreeGarbage(int ticketType,int cabinType){
        // 成人、儿童票
        if(ticketType==1){
            // 头等舱
            if(cabinType==1){
                weight+=40;
            }else if(cabinType==2){ // 公务舱
                weight+=30;
            }else{ // 经济舱
                weight+=20;
            }
        }else{ // 是婴儿票
            weight+=10;
        }
        return weight;
    }

    // 国内VIP可免费携带的行李重量
    public float InnerVIPFreeGarbage(int status){
        // 凤凰知音终身白金卡、白金卡
        if(status==2){
            additianalWeight+=30;
        }else if(status==3 || status==4) {
            additianalWeight += 20;
        }
        return additianalWeight;
    }

    public float getWeight() {
        return weight;
    }

    public float getAdditianalWeight() {
        return additianalWeight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public void setAdditianalWeight(float additianalWeight) {
        this.additianalWeight = additianalWeight;
    }
}
