package com.softwaretest.airplanecalculate.bussiness;

public class InterCarryFreeGarbage {
    private int num; // 免费行李件数
    private int weight; // 限重

    public InterCarryFreeGarbage() {
        num=0;
        weight=0;
    }

    public int InterFreeGarbage(int ticketType,int cabinType,int freeGarbageNum){
        // 若是头等舱、公务舱
        if(cabinType==1 || cabinType==2){
            // 若是成人、儿童票
            if(ticketType==1){
                num=2;
                weight=32;
            }
        }else if(cabinType==3){ // 若是悦享经济舱、超级经济舱
            num=2;
            weight=23;
        }else{ // 若是经济舱
            // 若是A类区域
            if(freeGarbageNum==1){
                num=1;
                weight=23;
            }else{
                num=2;
                weight=23;
            }
        }
        return weight;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
