package com.softwaretest.airplanecalculate.bussiness;

public class SpecialGarbageFee {
    private int fee; // 特殊行李花费

    public SpecialGarbageFee() {
        fee = 0;
    }

    public void getSpcialGarbageFee(int type,float weight){
        if(type==5){
            if(weight>=2 && weight<=23){
                fee+=2600;
            }else if(weight>23 && weight<=32){
                fee+=3900;
            }else if(weight>32 && weight<=45){
                fee+=5200;
            }
        }else if(type==6){
            if(weight>=2 && weight<=23){
                fee+=1300;
            }else if(weight>23 && weight<=32){
                fee+=2600;
            }else if(weight>32 && weight<=45){
                fee+=3900;
            }
        }else if(type==8){
            if(weight>=2 && weight<=23){
                fee+=490;
            }else if(weight>23 && weight<=32){
                fee+=3900;
            }
        }else if(type==9){
            if(weight>=2 && weight<=23){
                fee+=1300;
            }else if(weight>23 && weight<=32){
                fee+=2600;
            }
        }else if(type==10){
            if(weight>=2 && weight<=5){
                fee+=1300;
            }
        }else if(type==11){
            if(weight>=2 && weight<=8){
                fee+=3900;
            }else if(weight>8 && weight<=23){
                fee+=5200;
            }else if(weight>23 && weight<=32){
                fee+=7800;
            }
        }
    }

    public int getFee() {
        return fee;
    }
}
