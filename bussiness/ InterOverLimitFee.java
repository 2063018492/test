package com.softwaretest.airplanecalculate.bussiness;

public class InterOverLimitFee {
    private float fee = 0; // 超限花费

    // 超过重量或尺寸限制
    public void OverPropertyFee(float overWeight,float totSize,int overLimitRegion){
        if(overLimitRegion==1){
            // 尺寸不超限
            if(totSize>=60 && totSize<=158){
                if(overWeight>23 && overWeight<=28){
                    fee += 380;
                }else{
                    fee +=980;
                }
            }else{ // 尺寸超限
                if(overWeight>=2 && overWeight<=23){
                    fee +=980;
                }else{
                    fee += 1400;
                }
            }
        }else if(overLimitRegion==2){
            // 尺寸不超限
            if(totSize>=60 && totSize<=158){
                if(overWeight>23 && overWeight<=28){
                    fee += 280;
                }else{
                    fee +=690;
                }
            }else{ // 尺寸超限
                if(overWeight>=2 && overWeight<=23){
                    fee +=690;
                }else{
                    fee += 1100;
                }
            }
        }else if(overLimitRegion==3){
            if((overWeight>23 && overWeight<=32) || (totSize>158 && totSize<=203)){
                fee +=520;
            }else if(overWeight>23 && overWeight<=32 && totSize>158 && totSize<=203) {
                fee += 520;
            }
        }else if(overLimitRegion==4){
            // 尺寸不超限
            if(totSize>=60 && totSize<=158){
                if(overWeight>23 && overWeight<=28){
                    fee += 690;
                }else{
                    fee +=1040;
                }
            }else{ // 尺寸超限
                if(overWeight>=2 && overWeight<=23){
                    fee +=1040;
                }else{
                    fee += 2050;
                }
            }
        }else{
            // 尺寸不超限
            if(totSize>=60 && totSize<=158){
                if(overWeight>23 && overWeight<=28){
                    fee += 210;
                }else{
                    fee +=520;
                }
            }else{ // 尺寸超限
                if(overWeight>=2 && overWeight<=23){
                    fee +=520;
                }else{
                    fee += 830;
                }
            }
        }
    }

    // 超过件数限制
    public void OverNumFee(int type,int overNum){
        if(type==1){
            if(overNum==1){
                fee+=1400;
            }else if(overNum==2){
                fee+=2000;
            }else if(overNum>=3){
                fee+=3000;
            }
        }else if(type==2){
            if(overNum==1 || overNum==2){
                fee+=1100;
            }else if(overNum>=3){
                fee+=1590;
            }
        }else if(type==3){
            if(overNum==1 || overNum==2){
                fee+=1170;
            }else if(overNum>=3){
                fee+=1590;
            }
        }else if(type==4){
            if(overNum==1 || overNum==2){
                fee+=1380;
            }else if(overNum>=3){
                fee+=1590;
            }
        }else{
            if(overNum==1){
                fee+=830;
            }else if(overNum==2){
                fee+=1100;
            }else if(overNum>=3){
                fee+=1590;
            }
        }
    }

    public double getFee() {
        return fee;
    }
}
