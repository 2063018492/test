package com.softwaretest.airplanecalculate;

import com.softwaretest.airplanecalculate.bussiness.Handle;
import com.softwaretest.airplanecalculate.bussiness.InterCarryFreeGarbage;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

//@SpringBootTest
//public class InterOrdinaryGarbageTest {
//    private static InterCarryFreeGarbage interCarryFreeGarbage;
//
//    @BeforeAll
//    public static void Init(){
//        interCarryFreeGarbage = new InterCarryFreeGarbage();
//    }
//
//    @ParameterizedTest
//    @CsvFileSource(resources = "/InterOrdinaryGarbage.csv")
//    @DisplayName("计算国际普通行李花费")
//    public void test(int num,int freeWeight,int overLimitRegion,int totSize,int realWeight,float res){
//        // 初始化
//        interCarryFreeGarbage.setNum(num);
//        interCarryFreeGarbage.setWeight(freeWeight);
//
//        Handle handle = new Handle();
//        handle.setInterFreeGarbage(interCarryFreeGarbage);
//        assertEquals(handle.InterOrdinaryGarbage(overLimitRegion,totSize,realWeight),res);
//    }
//}

@org.testng.annotations.Test
public class InterOrdinaryGarbageTest {

    private static InterCarryFreeGarbage interCarryFreeGarbage;

    @BeforeClass
    public static void Init(){
        interCarryFreeGarbage = new InterCarryFreeGarbage();
    }

    @Test
    public void test1(){
        // -1,23,1,157,25,4380

        // 初始化
        interCarryFreeGarbage.setNum(-1);
        interCarryFreeGarbage.setWeight(23);

        Handle handle = new Handle();
        handle.setInterFreeGarbage(interCarryFreeGarbage);
        assertEquals(handle.InterOrdinaryGarbage(1,157,25),4380);
    }

    @Test
    public void test2(){
        // 1,32,1,160,30,980

        // 初始化
        interCarryFreeGarbage.setNum(1);
        interCarryFreeGarbage.setWeight(32);

        Handle handle = new Handle();
        handle.setInterFreeGarbage(interCarryFreeGarbage);
        assertEquals(handle.InterOrdinaryGarbage(1,160,30),980);
    }

    @Test
    public void test3(){
        // 1,23,1,100,20,0

        // 初始化
        interCarryFreeGarbage.setNum(1);
        interCarryFreeGarbage.setWeight(23);

        Handle handle = new Handle();
        handle.setInterFreeGarbage(interCarryFreeGarbage);
        assertEquals(handle.InterOrdinaryGarbage(1,100,20),0);
    }
}
