package com.softwaretest.airplanecalculate;

import com.softwaretest.airplanecalculate.bean.FormData;
import com.softwaretest.airplanecalculate.bean.GarbageInfo;
import com.softwaretest.airplanecalculate.bussiness.Handle;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.annotations.Test;
import org.testng.Assert;

//@SpringBootTest
//public class CalculateSpecialGarbageTest {
//    @ParameterizedTest
//    @CsvFileSource(resources = "/CalculateSpecialGarbage.csv")
//    @DisplayName("计算特殊行李费用的单元测试")
//    public void test(int requestType,int ticketType,int cabinType,int ill,int status,int price,int freeGarbageNum,int overLimitRegion,int type,float weight,String resStr,float resFlo){
//        // 初始化 1,1,1,2,1,1000,1,1,1,45,ok,0
//        FormData formData = new FormData(requestType,ticketType,cabinType,ill,status,price,freeGarbageNum,overLimitRegion,null);
//        Handle handle = new Handle(formData);
//        GarbageInfo garbageInfo = new GarbageInfo();
//        garbageInfo.setType(type);
//        garbageInfo.setWeight(weight);
//
//        assertEquals(handle.CalculateSpecialGarbage(garbageInfo),resStr);
//        assertEquals(handle.getFee(),resFlo);
//    }
//}

@org.testng.annotations.Test
public class CalculateSpecialGarbageTest {

    @Test
    public void test1(){
        FormData formData = new FormData(1,1,1,2,1,1000,1,1,null);
        Handle handle = new Handle(formData);
        GarbageInfo garbageInfo = new GarbageInfo();
        garbageInfo.setType(1);
        garbageInfo.setWeight(45);

        Assert.assertEquals(handle.CalculateSpecialGarbage(garbageInfo),"ok");
        Assert.assertEquals(handle.getFee(),0.0f);
    }

    @Test
    public void test2(){
        // 1,2,1,1,1,1000,1,1,2,100,ok,0
        FormData formData = new FormData(1,2,1,1,1,1000,1,1,null);
        Handle handle = new Handle(formData);
        GarbageInfo garbageInfo = new GarbageInfo();
        garbageInfo.setType(2);
        garbageInfo.setWeight(100);

        Assert.assertEquals(handle.CalculateSpecialGarbage(garbageInfo),"ok");
        Assert.assertEquals(handle.getFee(),0.0f);
    }

    @Test
    public void test3(){
        // 2,1,4,1,1,1000,1,1,7,33,特殊行李重量不符合规格,0
        FormData formData = new FormData(2,1,4,1,1,1000,1,1,null);
        Handle handle = new Handle(formData);
        GarbageInfo garbageInfo = new GarbageInfo();
        garbageInfo.setType(7);
        garbageInfo.setWeight(33);

        Assert.assertEquals(handle.CalculateSpecialGarbage(garbageInfo),"特殊行李重量不符合规格");
        Assert.assertEquals(handle.getFee(),0.0f);
    }

    @Test
    public void test4(){
        // 1,1,1,1,1,1000,1,1,4,100,ok,900
        FormData formData = new FormData(1,1,1,1,1,1000,1,1,null);
        Handle handle = new Handle(formData);
        GarbageInfo garbageInfo = new GarbageInfo();
        garbageInfo.setType(4);
        garbageInfo.setWeight(100);

        Assert.assertEquals(handle.CalculateSpecialGarbage(garbageInfo),"ok");
        Assert.assertEquals(handle.getFee(),900.0f);
    }

    @Test
    public void test5(){
        // 1,1,4,1,1,1000,1,1,11,3,ok,3900
        FormData formData = new FormData(1,1,4,1,1,1000,1,1,null);
        Handle handle = new Handle(formData);
        GarbageInfo garbageInfo = new GarbageInfo();
        garbageInfo.setType(11);
        garbageInfo.setWeight(3);

        Assert.assertEquals(handle.CalculateSpecialGarbage(garbageInfo),"ok");
        Assert.assertEquals(handle.getFee(),3900.0f);
    }
}
