package com.softwaretest.airplanecalculate;

import com.softwaretest.airplanecalculate.bussiness.InterOverLimitFee;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

//@SpringBootTest
//public class OverPropertyTest {
//    private static InterOverLimitFee interOverLimitFee;
//
//    @BeforeAll
//    public static void Init(){
//        interOverLimitFee = new InterOverLimitFee();
//    }
//
//    @ParameterizedTest
//    @CsvFileSource(resources = "/OverProperty.csv")
//    @DisplayName("计算国际航班超过重量或尺寸的收费")
//    public void test(float overWeight,float totSize,int overLimitRegion,int res){
//        interOverLimitFee.OverPropertyFee(overWeight,totSize,overLimitRegion);
//        assertEquals(interOverLimitFee.getFee(),res);
//    }
//}

@org.testng.annotations.Test
public class OverPropertyTest {
    private static InterOverLimitFee interOverLimitFee;

    @BeforeClass
    public static void Init(){
        interOverLimitFee = new InterOverLimitFee();
    }

    @Test
    public void test1(){
        // 25,100,1,380

        interOverLimitFee.OverPropertyFee(25,100,1);
        Assert.assertEquals(interOverLimitFee.getFee(),380.0d);
    }

    @Test
    public void test2(){
        // 30,100,2,1070

        interOverLimitFee.OverPropertyFee(30,100,2);
        Assert.assertEquals(interOverLimitFee.getFee(),1070.0d);
    }

    @Test
    public void test3(){
        // 20,200,4,2110

        interOverLimitFee.OverPropertyFee(20,200,4);
        Assert.assertEquals(interOverLimitFee.getFee(),2110.0d);
    }

    @Test
    public void test4(){
        // 30,200,5,2940

        interOverLimitFee.OverPropertyFee(30,200,5);
        Assert.assertEquals(interOverLimitFee.getFee(),2940.0d);
    }

    @Test
    public void test5(){
        // 30,200,3,3460

        interOverLimitFee.OverPropertyFee(30,200,3);
        Assert.assertEquals(interOverLimitFee.getFee(),3460.0d);
    }

    @Test
    public void test6(){
        // 20,150,3,3460

        interOverLimitFee.OverPropertyFee(20,150,3);
        Assert.assertEquals(interOverLimitFee.getFee(),3460.0d);
    }
}
