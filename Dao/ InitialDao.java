package com.softwaretest.airplanecalculate.dao;

import com.softwaretest.airplanecalculate.bean.CustomerStatus;
import com.softwaretest.airplanecalculate.bean.FreeGarbageRegion;
import com.softwaretest.airplanecalculate.bean.OverLimitRegion;
import com.softwaretest.airplanecalculate.bean.GarbageType;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InitialDao {
    public List<FreeGarbageRegion> getAllFreeGarbageRegion();
    public List<OverLimitRegion> getAllOverimitRegion();
    public List<GarbageType> getAllGarbageType();
    public List<CustomerStatus> getAllCustomerStatus();
}
