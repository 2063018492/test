package com.softwaretest.airplanecalculate.bean;

public class OverLimitRegion {
    private int type;
    private String region;

    public OverLimitRegion() {
    }

    public OverLimitRegion(int type, String region) {
        this.type = type;
        this.region = region;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
