package com.softwaretest.airplanecalculate.bean;

import java.util.List;

public class FormData {
    private int requestType; // 是国内还是国际，国内是1，国际是2

    private int ticketType; // 机票类型
    private int cabinType; // 客舱类型
    private int ill; // 是否有病在身

    // 国内
    private int status;// 乘客的VIP身份
    private int price; // 票价

    // 国际
    private int freeGarbageNum; // 免费携带的行李数量区域划分
    private int overLimitRegion; // 超限额收费区域划分

    // 行李信息
    private List<GarbageInfo> garbageInfoList; //所有行李的信息

    public FormData() {
    }

    public FormData(int requestType, int ticketType, int cabinType, int isIllness, int status, int price, int freeGarbageNum, int overLimitRegion, List<GarbageInfo> garbageInfoList) {
        this.requestType = requestType;
        this.ticketType = ticketType;
        this.cabinType = cabinType;
        this.ill = isIllness;
        this.status = status;
        this.price = price;
        this.freeGarbageNum = freeGarbageNum;
        this.overLimitRegion = overLimitRegion;
        this.garbageInfoList = garbageInfoList;
    }

    public int getRequestType() {
        return requestType;
    }

    public void setRequestType(int requestType) {
        this.requestType = requestType;
    }

    public int getTicketType() {
        return ticketType;
    }

    public void setTicketType(int ticketType) {
        this.ticketType = ticketType;
    }

    public int getCabinType() {
        return cabinType;
    }

    public void setCabinType(int cabinType) {
        this.cabinType = cabinType;
    }

    public int getIll() {
        return ill;
    }

    public void setIll(int ill) {
        this.ill = ill;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getFreeGarbageNum() {
        return freeGarbageNum;
    }

    public void setFreeGarbageNum(int freeGarbageNum) {
        this.freeGarbageNum = freeGarbageNum;
    }

    public int getOverLimitRegion() {
        return overLimitRegion;
    }

    public void setOverLimitRegion(int overLimitRegion) {
        this.overLimitRegion = overLimitRegion;
    }

    public List<GarbageInfo> getGarbageInfoList() {
        return garbageInfoList;
    }

    public void setGarbageInfoList(List<GarbageInfo> garbageInfoList) {
        this.garbageInfoList = garbageInfoList;
    }
}
