package com.softwaretest.airplanecalculate.bean;

public class GarbageType {
    private int type;
    private String info;

    public GarbageType() {
    }

    public GarbageType(int type, String info) {
        this.type = type;
        this.info = info;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
