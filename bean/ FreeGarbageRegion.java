package com.softwaretest.airplanecalculate.bean;

public class FreeGarbageRegion {
    private String type;
    private String region;

    public FreeGarbageRegion() {
    }

    public FreeGarbageRegion(String type, String region) {
        this.type = type;
        this.region = region;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
