package com.softwaretest.airplanecalculate.bean;

public class GarbageInfo {
    private int type; // 类型
    private float length; // 长
    private float width; // 宽
    private float height; // 高
    private float weight; // 重

    public GarbageInfo() {
    }

    public GarbageInfo(int type, float length, float width, float height, float weight) {
        this.type = type;
        this.length = length;
        this.width = width;
        this.height = height;
        this.weight = weight;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }
}
