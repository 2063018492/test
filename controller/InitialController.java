package com.softwaretest.airplanecalculate.controller;

import com.alibaba.fastjson.JSON;
import com.softwaretest.airplanecalculate.bean.CustomerStatus;
import com.softwaretest.airplanecalculate.bean.FreeGarbageRegion;
import com.softwaretest.airplanecalculate.bean.OverLimitRegion;
import com.softwaretest.airplanecalculate.bean.GarbageType;
import com.softwaretest.airplanecalculate.dao.InitialDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

@RestController
public class InitialController {

    @Autowired // 让它自动注入
    InitialDao initial;

    @CrossOrigin
    @RequestMapping("/InitialInter")
    public String initialInter(){
        String status = "error";
        List<GarbageType> garbageTypeList = initial.getAllGarbageType();
        List<FreeGarbageRegion> freeGarbageRegionList = initial.getAllFreeGarbageRegion();
        List<OverLimitRegion> overLimitRegionList = initial.getAllOverimitRegion();

        if(garbageTypeList!=null && freeGarbageRegionList!=null && overLimitRegionList!=null){
            status = "ok";
        }

        HashMap<String, Object> res = new HashMap<>();
        res.put("status",status);
        res.put("garbageTypeList",garbageTypeList);
        res.put("freeGarbageRegionList",freeGarbageRegionList);
        res.put("overLimitRegionList",overLimitRegionList);
        String resJson = JSON.toJSONString(res);
        return resJson;
    }

    @CrossOrigin
    @RequestMapping("/InitialInner")
    public String initialInner(){
        String status = "error";
        List<GarbageType> garbageTypeList = initial.getAllGarbageType();
        List<CustomerStatus> customerStatusList = initial.getAllCustomerStatus();

        if(garbageTypeList!=null && customerStatusList!=null){
            status = "ok";
        }
        System.out.println(garbageTypeList);

        HashMap<String, Object> res = new HashMap<>();
        res.put("status",status);
        res.put("garbageTypeList",garbageTypeList);
        res.put("customerStatusList",customerStatusList);
        String resJson = JSON.toJSONString(res);
        return resJson;
    }
}
