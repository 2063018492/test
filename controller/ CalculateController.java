package com.softwaretest.airplanecalculate.controller;

import com.alibaba.fastjson.JSON;
import com.softwaretest.airplanecalculate.bean.FormData;
import com.softwaretest.airplanecalculate.bussiness.Handle;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class CalculateController {

    @CrossOrigin
    @RequestMapping("/calculate")
    public String Handle(@RequestBody FormData formData){
        //定义处理函数的对象
        Handle handle = new Handle(formData);

        // 开始计算
        String status = handle.Calculate();
        float fee = handle.getFee();

        HashMap<String, Object> res = new HashMap<>();
        res.put("status",status);
        res.put("fee",fee);
        String resJson = JSON.toJSONString(res);
        return resJson;
    }
}
